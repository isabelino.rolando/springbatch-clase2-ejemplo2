package com.formacionspring.web.model;

public class Persona {

	private long id;
	private String nombre;
	private String apellido;
	private String email;
 
	//metodo estatico para devolver un array de string
	public static String[] fields(){
		return new String[] {"id","nombre","apellido","email"};
	}
	
	public Persona() {
		super();
	}

	public Persona(long id, String nombre, String apellido, String email) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	
	
}
