package com.formacionspring.web.config;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import com.formacionspring.web.model.Persona;
//Anotación encargada de definir que la clase es una clase de configuración para el framework
@Configuration
//proporciona una configuración básica para crear trabajos por lotes.
//Dentro de esta configuración base, se crea una instancia de StepScope 
//además de una cantidad de beans disponibles para ser conectados automáticamente
@EnableBatchProcessing
public class EjemploBatchConfiguracion {

	//objeto constructor step
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	
	//objeto constructor job
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	
	//objeto de instancia a conexion a base de datos
	@Autowired
	private DataSource dataSource;
	
	
	
	
	
	//metodo para manejar la lectura del archivo csv
	//agregamos el @Bean para que este metodo se agrege al repositorio de spring
	@Bean
	public FlatFileItemReader<Persona> readFromCsv(){
		//creamos un objeto de tipo FlatFileItemReader clase de spring batch para la lectura
		//que nos ayuda a pasar los datos leidos desde el archivo csv al objeto persona
		FlatFileItemReader<Persona> reader = new FlatFileItemReader<Persona>();
		//si tenemos titulos de columnas en nuestros archivos csv lo podemos ignorar con esta linea
		//reader.setLinesToSkip(1);//con esto ignora la primera fila en caso de tener titulos
		
		//con el setResource hacemos referencia al archivo
		reader.setResource(new ClassPathResource("persona.csv"));
	 
	
		//mapeamos todos los campos desde el archivo 
		//lo preparamos para cargarlos a traves de la clase persona
		reader.setLineMapper(new DefaultLineMapper<Persona>() {
			{
				//hacemos referencia a cada atributo de la clase con el metodo fields
			setLineTokenizer(new DelimitedLineTokenizer() {
				{
					setNames(Persona.fields());
				}
			});
			//hacemos referencia a la clase persona
			setFieldSetMapper(new BeanWrapperFieldSetMapper<Persona>() {
				{
					setTargetType(Persona.class);
				}
			});
			
			}
		});
		//finalmente retornamos el objeto de lectura
		return reader;
	}
	
	
	
	//metodo para interactuar con la base de datos
	//metodo para insertar en la base de datos 
	//agregamos el @Bean para que este metodo se agrege al repositorio de spring
	@Bean
	public JdbcBatchItemWriter<Persona>writerIntoDB(){
		//creamos el objeto JdbcBatchItemWriter de escritura
		//para cargar los datos leidos a la base de datos 
		JdbcBatchItemWriter<Persona> writer = new JdbcBatchItemWriter<Persona>();
		//hacemos referencia a las instancias de conexion a bd que establecimos en application.properties
		writer.setDataSource(dataSource);
		//agregamos el metodo setSQL con el query de insercion de datos necesario
		writer.setSql("insert into persona(id,nombre,apellido,email) values(:id,:nombre,:apellido,:email);");
		//el setItemSqlParameterSourceProvider es necesario cuando se usan parámetros con nombre para
		//la instrucción SQL y el tipo que se va a escribir no implementa Map.
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Persona>());
		//finalmente retornamos la escritura
		return writer;
	}
	
	

	
	//metodo de los pasos o procesos a ejecutar
	//agregamos el @Bean para que este metodo se agrege al repositorio de spring
	@Bean
	public Step step() {
		//retornmaos la construccion del paso, haciendo referencia a 10 unidades de procesamiento
		//en donde estaria realizando la lectura y escritura programada en cada metodo readFromCsv() y writerIntoDB()
		return stepBuilderFactory.get("step").<Persona,Persona>chunk(10).reader(readFromCsv()).writer(writerIntoDB()).build();
	}
	

	
	//metodo para definir como se procesaran los steps
	//agregamos el @Bean para que este metodo se agrege al repositorio de spring
	@Bean
	public Job job() {
		//return jobBuilderFactory.get("job").start(step()).next(step2()).build();
		//return jobBuilderFactory.get("job").flow(step()).next(step2()).end().build();
	
		return jobBuilderFactory.get("job").flow(step()).end().build();
		//return jobBuilderFactory.get("job").start(step()).next(step2()).build();
	}
	
	
}
