package com.formacionspring.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ejemplo1SpringbApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ejemplo1SpringbApplication.class, args);
	}

}
